<!-- #include file="~/ReplicatedCode.aspx" -->
<!DOCTYPE html>
<html>
	<head>
		<title>DotDotSmile | Dresses &amp; Rompers for Toddlers &amp; Infants</title>
		<!-- build:css css/combined.css -->
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<!-- endbuild -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
<body>
<header>
	<a href="index.aspx">
		<img class="logo" src="img/logo.png">
	</a>	
	<nav>
		<ul>
			<li>
				<a class="a1" href="MemberToolsDotNet/ShoppingCartv4/StartPublicShopping.aspx?referringDealerId=<%=ReferringDealerID%>">shop</a>
			</li>
			<li>
				<a class="a2" href="about.aspx">about us</a>
			</li>
			<li>
				<a class="a3" href="contact.aspx">contact</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/EnrollmentNew/StartPublicEnrollment.aspx?referringDealerId=<%=ReferringDealerID%>">become a merchandiser</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/Login/FirestormLogin.aspx">login</a>
			</li>
		</ul>
	</nav>
</header>
<main class="return">
	<div class="container margin-top-sm">
		<h3>Direct Consumer Return Policy</h3>

		<p>DotDotSmile, LLC. (“DDS”) stands behind the quality and craftsmanship of our products. Each of our patterns and styles are designed to ensure the highest comfort and satisfaction. If for any reason you feel like your purchase made directly from DDS does not meet your expectations, we are committed to finding a solution that will bring confidence and value to your DDS purchase. DDS offers a simple four-step process to return DDS products in the unlikely event you receive them damaged or defective:</p>
		<ol>
			<li>Email DDS and explain your return request at: <a href="mailto:returns@dotdotsmile.com">returns@dotdotsmile.com</a>. Please include any supporting information about the return request (photos, receipts, and purchase information).</li>
			<li>DDS will notify you by email when your return request is approved.</li>
			<li>DDS will email you a document describing the items that will need to be included with the return along with an electronic shipping label to print out and attach to an appropriately sized shipping container.</li>
			<li>Please allow 7-10 days for the DDS warehouse to receive your package, process the request, and return your replacement products.</li>
		</ol>
		<ul>
			<li>DDS will cover all shipping on approved return requests. Any returns sent to DDS before approval is granted will be at the expense of the consumer including any costs to return unapproved items. </li>
		</ul>

		&nbsp;

		<strong>Non-Returnable / Non-Exchangeable Products</strong>
		<ol>
			<li>Products missing original tags and packaging</li>
			<li>Products with visible signs of wear or tear</li>
			<li>Products that were not kept in a smoke/odor free environment</li>
			<li>Products that were purchased more than 30 days prior to the date of the return request</li>
			<li>Products that were not purchased directly from DDS (please contact the person or business from whom you purchased the product)</li>
		</ol>

		&nbsp;

		<p><strong>PLEASE NOTE:</strong> Due to the limited run of each print and style, DDS may not be able to replace the exact item purchased. If a substitute product is rejected, a refund may be requested. Please allow 7-10 days for any refund to process. DDS may adjust or amend this Return Policy, at any time, and to comply with all federal, state, and local laws and regulations in the conduct of their businesses.</p>
	</div>
</main>
<footer>
	<div class="social-bar">
		<a href="https://www.instagram.com/dotdotsmile/" target='_blank'class='social-icon'>
			<i class="fa fa-instagram"></i>
		</a>
		<a href="https://www.facebook.com/dotdotsmilekids/" target='_blank' class='social-icon'>
			<i class="fa fa-facebook"></i>
		</a>
		<a href="https://twitter.com/DotDotSmileMar" target='_blank' class='social-icon'>
			<i class="fa fa-twitter"></i>
		</a>
	</div>

	<div class="copy">
		DotDotSmile © 2017. All Rights Reserved. Powered by <a href="http://www.lemonadestand.org" target="_blank">Lemonade Stand</a>.
	</div>

	<div class="footer-nav">
		<ul>
			<li>
				<a href="return.aspx">return policy</a>
			</li>
			<li>
				<a href="privacy.aspx">privacy policy</a>
			</li>
			<li>
				<a href="terms.aspx">terms &amp; conditions</a>
			</li>
		</ul>
	</div>

	
</footer>
</body>
	<!-- build:js scripts/combined.js -->
	<script type="text/javascript" src='js/vendor.js'></script>
	<script type="text/javascript" src="js/script.js"></script>
	<!-- endbuild -->
</html>