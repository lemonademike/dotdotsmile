<!-- #include file="~/ReplicatedCode.aspx" -->
<!DOCTYPE html>
<html>
	<head>
		<title>DotDotSmile | Dresses &amp; Rompers for Toddlers &amp; Infants</title>
		<!-- build:css css/combined.css -->
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<!-- endbuild -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-90642525-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-90642525-1');
</script>

	</head>
<body>
<header>
	<div id="counter"></div>

	<a href="index.aspx">
		<img class="logo" src="img/logo.png">
	</a>	
	<nav>
		<ul>
			<li>
				<a class="a1" href="MemberToolsDotNet/ShoppingCartv4/StartPublicShopping.aspx?referringDealerId=<%=ReferringDealerID%>">shop</a>
			</li>
			<li>
				<a class="a2" href="about.aspx">about us</a>
			</li>
			<li>
				<a class="a3" href="contact.aspx">contact</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/EnrollmentNew/StartPublicEnrollment.aspx?referringDealerId=<%=ReferringDealerID%>">become a merchandiser</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/Login/FirestormLogin.aspx">login</a>
			</li>
		</ul>
	</nav>
</header>
<main>
	<div class="container">
		<div class="video-container">
			<iframe class='dotdotvideo' src="https://www.youtube.com/embed/ST50Z8nk6AY?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>
		</div>
		<div class="row margin-top-lg align-items-center">
			<div class="col-0 col-lg-2"></div>
			<div class="col-12 col-sm-6 col-lg-4">
				<img class='fam-photo w-100' src="img/thompson-family.jpg">
			</div>
			<div class="col-12 col-sm-6">
				<div class="we-highlight">
					We 
					<div class="dot dot-aqua"></div> 
					<div class="dot dot-pink"></div> 
					<div class="dot dot-yellow"></div>
				</div>
				<div class="we-text">
					<div class="we-row">
						We are <span class='fun-highlight fun-blue'>Jeff</span> and <span class='fun-highlight fun-pink'>Nicole</span>.
					</div>
					<div class="we-row">
						We are from <span class='fun-highlight fun-orange'>California</span>.
					</div>
					<div class="we-row">
						We are in <span class='fun-highlight fun-red'>love</span>.
					</div>
					<div class="we-row">
						We love being <span class='fun-highlight fun-aqua'>parents</span>.
					</div>
					<div class="we-row">
						We especially love our <span class='fun-highlight fun-purple'>babies</span>: Lucy, Azure and Lennon.
					</div>
					<div class="we-row">
						We make <span class='fun-highlight fun-orange'>silly</span> faces.
					</div>
					<div class="we-row">
						We <span class='fun-highlight fun-red'>love</span> what we do.
					</div>
					<div class="we-row">
						We live for <span class='fun-highlight fun-yellow'>sweets</span>.
					</div>
					<div class="we-row">
						We are <span class='fun-highlight fun-green'>entrepreneurs</span>.
					</div>
					<div class="we-row">
						We are a <span class='fun-highlight fun-blue'>family</span>.
					</div>
					<div class="we-row">
						We love to <span class='fun-highlight fun-pink'>smile</span> and spread it around.
					</div>					
				</div>				
			</div>	
		</div>
	</div>
</main>
<footer>
	<div class="social-bar">
		<a href="https://www.instagram.com/dotdotsmile/" target='_blank'class='social-icon'>
			<i class="fa fa-instagram"></i>
		</a>
		<a href="https://www.facebook.com/dotdotsmilekids/" target='_blank' class='social-icon'>
			<i class="fa fa-facebook"></i>
		</a>
	</div>

	<div class="copy">
		DotDotSmile © 2017. All Rights Reserved. Powered by <a href="http://www.lemonadestand.org" target="_blank">Lemonade Stand</a>.
	</div>

	<div class="footer-nav">
		<ul>
			<li>
				<a href="return.aspx">return policy</a>
			</li>
			<li>
				<a href="privacy.aspx">privacy policy</a>
			</li>
			<li>
				<a href="terms.aspx">terms &amp; conditions</a>
			</li>
		</ul>
	</div>

	
</footer>
</body>
	<!-- build:js scripts/combined.js -->
	<script type="text/javascript" src='js/vendor.js'></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/countdown.js"></script>
	<!-- endbuild -->
</html>