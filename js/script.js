$(".menu-btn").click(function(){
	$("nav").slideToggle("show");
});

$('#main-carousel').owlCarousel({
	animateOut: 'fadeOut',
    items:1,
    margin:0,
    stagePadding:0,
    smartSpeed:450,
    loop: true,
    dots: true,
    nav: true,
    mouseDrag: false,
    autoplay: true,
    autoplayHoverPause: true,
    navText:["<i class='fa fa-chevron-left' aria-hidden='true'></i>","<i class='fa fa-chevron-right' aria-hidden='true'></i>"]
});



