<!-- #include file="~/ReplicatedCode.aspx" -->
<!DOCTYPE html>
<html>
	<head>
		<title>DotDotSmile | Dresses &amp; Rompers for Toddlers &amp; Infants</title>
		<!-- build:css css/combined.css -->
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<!-- endbuild -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-90642525-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-90642525-1');
</script>

	</head>
<body>
<header>
	<div id="counter"></div>

	<a href="index.aspx">
		<img class="logo" src="img/logo.png">
	</a>	
	<nav>
		<ul>
			<li>
				<a class="a1" href="MemberToolsDotNet/ShoppingCartv4/StartPublicShopping.aspx?referringDealerId=<%=ReferringDealerID%>">shop</a>
			</li>
			<li>
				<a class="a2" href="about.aspx">about us</a>
			</li>
			<li>
				<a class="a3" href="contact.aspx">contact</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/EnrollmentNew/StartPublicEnrollment.aspx?referringDealerId=<%=ReferringDealerID%>">become a merchandiser</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/Login/FirestormLogin.aspx">login</a>
			</li>
		</ul>
	</nav>
</header>
<main>
	<div class="container">
		<div class="row contact-wrap">
			<div class="col-md-5">
				<img src="img/on-the-fence-square.jpg" alt="little girl on stool"/>
			</div>
			<div class="col-md-7">
				<form class='contact-form'>
					<div class="row">
						<div class="col-12 col-sm-6">
							<input type="text" name="name" placeholder="name" required>
						</div>
						<div class="col-12 col-sm-6">
							<input type="email" name="email" placeholder="email" required>
						</div>
						<div class="col-12">
							<textarea name='message' rows="4"></textarea>
						</div>
					</div>
					<input class='sub-btn' type="submit" name="submit" value="send">
				</form>
				<p class="contact-info">EMAIL // <a href="mailto:Info@dotdotsmile.com" class='social-icon'>Info@dotdotsmile.com</a></p>
				<p class="contact-info">PHONE // <a href="tel:+19514963764">951-496-3764</a></p>
			</div>
		</div>
	</div>
</main>
<footer>
	<div class="social-bar">
		<a href="https://www.instagram.com/dotdotsmile/" target='_blank'class='social-icon'>
			<i class="fa fa-instagram"></i>
		</a>
		<a href="https://www.facebook.com/dotdotsmilekids/" target='_blank' class='social-icon'>
			<i class="fa fa-facebook"></i>
		</a>
	</div>

	<div class="copy">
		DotDotSmile © 2017. All Rights Reserved. Powered by <a href="http://www.lemonadestand.org" target="_blank">Lemonade Stand</a>.
	</div>

	<div class="footer-nav">
		<ul>
			<li>
				<a href="return.aspx">return policy</a>
			</li>
			<li>
				<a href="privacy.aspx">privacy policy</a>
			</li>
			<li>
				<a href="terms.aspx">terms &amp; conditions</a>
			</li>
		</ul>
	</div>

	
</footer>
</body>
	<!-- build:js scripts/combined.js -->
	<script type="text/javascript" src='js/vendor.js'></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/countdown.js"></script>
	<!-- endbuild -->
</html>