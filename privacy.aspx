<!-- #include file="~/ReplicatedCode.aspx" -->
<!DOCTYPE html>
<html>
	<head>
		<title>DotDotSmile | Dresses &amp; Rompers for Toddlers &amp; Infants</title>
		<!-- build:css css/combined.css -->
		<link rel="stylesheet" type="text/css" href="css/styles.css">
		<!-- endbuild -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	</head>
<body>
<header>
	<a href="index.aspx">
		<img class="logo" src="img/logo.png">
	</a>	
	<nav>
		<ul>
			<li>
				<a class="a1" href="MemberToolsDotNet/ShoppingCartv4/StartPublicShopping.aspx?referringDealerId=<%=ReferringDealerID%>">shop</a>
			</li>
			<li>
				<a class="a2" href="about.aspx">about us</a>
			</li>
			<li>
				<a class="a3" href="contact.aspx">contact</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/EnrollmentNew/StartPublicEnrollment.aspx?referringDealerId=<%=ReferringDealerID%>">become a merchandiser</a>
			</li>
			<li>
				<a class="a3" href="MemberToolsDotNet/Login/FirestormLogin.aspx">login</a>
			</li>
		</ul>
	</nav>
</header>
<main class="privacy">
	<div class="container margin-top-sm">
		<h2>privacy policy</h2>
		<div class="entry-content" itemprop="text"><p><span>This Privacy Policy governs the manner in which DotDotSmile collects, uses, maintains and discloses information collected from users (each, a "User") of the&nbsp;<a href="https://www.dotdotsmile.com/">https://www.dotdotsmile.com/</a>&nbsp;website ("Site"). This privacy policy applies to the Site and all products and services offered by DotDotSmile.</span></p>
		<p><span><strong>Personal identification information</strong><br>
		We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, subscribe to the newsletter, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</span></p>
		<p><span><strong>Non-personal identification information</strong><br>
		We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</span></p>
		<p><span><strong>Web browser cookies</strong><br>
		Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</span></p>
		<p><span><strong>How we use collected information</strong><br>
		DotDotSmile may collect and use Users personal information for the following purposes:</span></p>
		<ul>
		<li><span><i>To improve customer service</i><br>
		Information you provide helps us respond to your customer service requests and support needs more efficiently.</span></li>
		<li><span><i>- To personalize user experience</i><br>
		We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</span></li>
		<li><span><i>To improve our Site</i><br>
		We may use feedback you provide to improve our products and services.</span></li>
		<li><span><i>To process payments</i><br>
		We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.</span></li>
		<li><span><i>To run a promotion, contest, survey or other Site feature</i><br>
		To send Users information they agreed to receive about topics we think will be of interest to them.</span></li>
		<li><span><i>To send periodic emails</i><br>
		We may use the email address to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, they may do so by contacting us via our Site.</span></li>
		</ul>
		<p><span><strong>How we protect your information</strong><br>
		We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</span></p>
		<p><span><strong>Sharing your personal information</strong><br>
		We do not sell, trade, or rent Users' personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above. We may use third-party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</span></p>
		<p><span><strong>Third party websites</strong><br>
		Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website's own terms and policies.</span></p>
		<p><span>As you browse https://www.dotdotsmile.com/, advertising cookies will be placed on your computer so that we can understand what you are interested in. Our display advertising partner, AdRoll, then enables us to present you with retargeting advertising on other sites based on your previous interaction with https://www.dotdotsmile.com/. The techniques our partners employ do not collect personal information such as your name, email address, postal address or telephone number. You can visit this <a href="http://www.networkadvertising.org/choices/" target="_blank">page</a> to opt out of AdRoll and their partners' targeted advertising.</span></p>
		<p><span><strong>Changes to this privacy policy</strong><br>
		DotDotSmile has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</span></p>
		<p><span><strong>Your acceptance of these terms</strong><br>
		By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</span></p>
		<p><span data-ctm-watch-id="3" data-ctm-tracked="1" data-ctm-remark="1"><strong>Contacting us</strong><br>
		If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br>
		<a href="https://www.dotdotsmile.com/">DotDotSmile</a><br>
		<a href="https://www.dotdotsmile.com/">https://www.dotdotsmile.com/</a><br>
		 1141 Pomona Rd, Unit A, Corona, CA 92882 United States<br>
		(951) 463-4132<br>
		info@dotdotsmile.com</span></p>
		<p><span><em>This document was last updated on November 8, 2016.</em></span></p>
		</div>
	</div>
</main>
<footer>
	<div class="social-bar">
		<a href="https://www.instagram.com/dotdotsmile/" target='_blank'class='social-icon'>
			<i class="fa fa-instagram"></i>
		</a>
		<a href="https://www.facebook.com/dotdotsmilekids/" target='_blank' class='social-icon'>
			<i class="fa fa-facebook"></i>
		</a>
		<a href="https://twitter.com/DotDotSmileMar" target='_blank' class='social-icon'>
			<i class="fa fa-twitter"></i>
		</a>
	</div>

	<div class="copy">
		DotDotSmile © 2017. All Rights Reserved. Powered by <a href="http://www.lemonadestand.org" target="_blank">Lemonade Stand</a>.
	</div>

	<div class="footer-nav">
		<ul>
			<li>
				<a href="return.aspx">return policy</a>
			</li>
			<li>
				<a href="privacy.aspx">privacy policy</a>
			</li>
			<li>
				<a href="terms.aspx">terms &amp; conditions</a>
			</li>
		</ul>
	</div>

	
</footer>
</body>
	<!-- build:js scripts/combined.js -->
	<script type="text/javascript" src='js/vendor.js'></script>
	<script type="text/javascript" src="js/script.js"></script>
	<!-- endbuild -->
</html>